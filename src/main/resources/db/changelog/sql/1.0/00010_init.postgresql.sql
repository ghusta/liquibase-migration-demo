-- liquibase formatted sql

-- See doc : https://docs.liquibase.com/concepts/changelogs/sql-format.html

-- changeset husta_g:1648477655315-1
-- precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM information_schema.tables where table_name = 'country'
-- comment: Database initialisation
CREATE TABLE "country"
(
    "code" CHAR(3) NOT NULL,
    "name" TEXT NOT NULL,
    "continent" TEXT NOT NULL, 
    "region" TEXT NOT NULL,
    "surface_area" FLOAT4 NOT NULL,
    "indep_year" SMALLINT,
    "population" INTEGER NOT NULL,
    "life_expectancy" FLOAT4,
    "gnp" numeric(10, 2),
    "gnp_old" numeric(10, 2),
    "local_name" TEXT NOT NULL,
    "government_form" TEXT NOT NULL,
    "head_of_state" TEXT,
    "capital" INTEGER,
    "code2" CHAR(2) NOT NULL,
    CONSTRAINT "country_pkey" PRIMARY KEY ("code")
);
COMMENT ON COLUMN "country"."gnp" IS 'GNP is Gross national product';

CREATE TABLE "city" ("id" INTEGER NOT NULL, "name" TEXT NOT NULL, "country_code" CHAR(3) NOT NULL, "district" TEXT NOT NULL, "population" INTEGER NOT NULL, CONSTRAINT "city_pkey" PRIMARY KEY ("id"));

CREATE TABLE "country_language" ("country_code" CHAR(3) NOT NULL, "language" TEXT NOT NULL, "is_official" BOOLEAN NOT NULL, "percentage" FLOAT4 NOT NULL, CONSTRAINT "country_language_pkey" PRIMARY KEY ("country_code", "language"));

ALTER TABLE "country" ADD CONSTRAINT "country_capital_fkey" FOREIGN KEY ("capital") REFERENCES "city" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "city" ADD CONSTRAINT "country_fk" FOREIGN KEY ("country_code") REFERENCES "country" ("code") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "country_language" ADD CONSTRAINT "country_language_country_code_fkey" FOREIGN KEY ("country_code") REFERENCES "country" ("code") ON UPDATE NO ACTION ON DELETE NO ACTION;
-- rollback DROP TABLE "country", "country_language", "city"