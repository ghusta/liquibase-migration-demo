package org.example;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.Scope;
import liquibase.changelog.ChangeSet;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.configuration.ConfiguredValue;
import liquibase.configuration.LiquibaseConfiguration;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.integration.commandline.LiquibaseCommandLineConfiguration;
import liquibase.logging.LogService;
import liquibase.logging.core.JavaLogService;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import liquibase.util.LiquibaseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

/**
 * See : https://docs.liquibase.com/workflows/liquibase-community/using-liquibase-java-api.html
 */
public class LiquibaseInitialiser {

    protected static final Logger logger = LoggerFactory.getLogger(LiquibaseInitialiser.class);

    private String changeLogFile;
    private Liquibase liquibase = null;
    private Database database = null;
    private ResourceAccessor resourceAccessor;

    private Properties liquibaseProperties;

    static {
        initLogService();
    }

    public static void main(String[] args) {
        try {
            LiquibaseInitialiser initialiser = new LiquibaseInitialiser();

//            initialiser.loadConfig();

            logger.info("Trying to update DB...");
            initialiser.update();
        } catch (LiquibaseException | IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public LiquibaseInitialiser() throws LiquibaseException, IOException {
        final String liquibaseBuildVersion = getLiquibaseBuildVersion();

        liquibaseProperties = new Properties();
        liquibaseProperties.load(new FileReader("liquibase.properties"));
        assert (!liquibaseProperties.isEmpty());

        changeLogFile = liquibaseProperties.getProperty("changeLogFile");
        assert (changeLogFile != null);
        resourceAccessor = new ClassLoaderResourceAccessor();
    }

    /**
     * @see Liquibase
     */
    public void update() throws SQLException, LiquibaseException {
        // https://docs.liquibase.com/concepts/connections/creating-config-properties.html
        String jdbcUrl = liquibaseProperties.getProperty("url");
        assert (jdbcUrl != null);
        String jdbcUsername = liquibaseProperties.getProperty("username");
        assert (jdbcUsername != null);
        String jdbcPassword = liquibaseProperties.getProperty("password");
        assert (jdbcPassword != null);
        String defaultSchemaName = liquibaseProperties.getProperty("defaultSchemaName");
        String liquibaseSchemaName = liquibaseProperties.getProperty("liquibaseSchemaName");

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(jdbcUrl, jdbcUsername, jdbcPassword);
            database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(conn));
            // database.setDefaultSchemaName(defaultSchemaName);
            // database.setLiquibaseSchemaName(liquibaseSchemaName);
            String databaseProductName = database.getDatabaseProductName();
            String databaseProductVersion = database.getDatabaseProductVersion();
            logger.info("Database : {} {}", databaseProductName, databaseProductVersion);

            liquibase = new Liquibase(changeLogFile, resourceAccessor, database);
            DatabaseChangeLog databaseChangeLog = liquibase.getDatabaseChangeLog();
            Contexts contexts = new Contexts(liquibaseProperties.getProperty("contexts"));
            LabelExpression labelExpression = new LabelExpression();

            List<ChangeSet> unrunChangeSets = liquibase.listUnrunChangeSets(contexts, labelExpression);
            logger.info("Count unrun Changesets = {}", unrunChangeSets.size());

            liquibase.update(contexts, labelExpression);
        } finally {
            if (liquibase != null) {
                liquibase.close();
            } else if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     * @see LiquibaseConfiguration
     */
    public void loadConfig() {
        final ConfiguredValue<String> defaultsFileConfig = LiquibaseCommandLineConfiguration.DEFAULTS_FILE.getCurrentConfiguredValue();
        final File defaultsFile = new File(defaultsFileConfig.getValue());

        final LiquibaseConfiguration liquibaseConfiguration = Scope.getCurrentScope().getSingleton(LiquibaseConfiguration.class);
        logger.info("End loadConfig()");
    }

    private static String getLiquibaseBuildVersion() {
        return LiquibaseUtil.getBuildVersion();
    }

    private static void initLogService() {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] %4$s [%2$s] %5$s%6$s%n");
        java.util.logging.Logger liquibaseLogger = java.util.logging.Logger.getLogger("liquibase");
        Scope currentScope = Scope.getCurrentScope();
        LogService logService = currentScope.get(Scope.Attr.logService, LogService.class);
        if (logService instanceof JavaLogService) {
            ((JavaLogService) logService).setParent(liquibaseLogger);
        }
    }

}
